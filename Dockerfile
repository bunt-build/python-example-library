FROM python:3.8
WORKDIR /code/
ENV PYTHONPATH=/code/:$PYTHONPATH

RUN pip3 install flask==1.1.1

COPY library /code/library

ENTRYPOINT ["python3"]
CMD ["-m", "library"]
