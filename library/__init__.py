from flask import Flask, request, abort, jsonify
from dataclasses import dataclass
from typing import List, Optional
import itertools
from library.book import Book

app = Flask(__name__)
book_id_maker = itertools.count()
books: List[Book] = []


def book_find(book_id):
  try:
    book_id: int = int(book_id)
  except ValueError:
    raise abort(400, 'Book id must be an integer')

  for book in books:
    if book.id == book_id:
      return book

  raise abort(404, 'Book not found')


@app.route('/books', methods=['GET'])
def book_list():
  return jsonify([book.to_dict() for book in books])


@app.route('/books', methods=['POST'])
def book_create():
  book = Book.from_dict(request.json)

  if book.id:
    raise abort(400, 'Cannot provide ID for creation')

  if not book.title:
    return abort(400, 'Must provide title of book when creating a book')

  if not book.author:
    return abort(400, 'Must provide author of book when creating a book')

  book.id = next(book_id_maker)
  books.append(book)
  return jsonify(book.to_dict())


@app.route('/books/<book_id>', methods=['PUT'])
def book_update(book_id: str):
  old = book_find(book_id)
  book = Book.from_dict(request.json)

  if book.id and (book.id != old.id):
    raise abort(400, 'Cannot change book ID with an update')

  new = Book(
    id=old.id,
    title=book.title or old.title,
    author=book.author or old.author,
  )

  books.remove(old)
  books.append(new)

  return jsonify(new.to_dict())

@app.route('/books/<book_id>', methods=['GET'])
def book_get(book_id: str):
  return jsonify(book_find(book_id).to_dict())

@app.route('/books/<book_id>', methods=['HEAD'])
def book_exists(book_id: str):
  book_find(book_id)
  return '', 200

@app.route('/books/<book_id>', methods=['DELETE'])
def book_delete(book_id: str):
  book = book_find(book_id)
  books.remove(book)
  return jsonify(book.to_dict())
