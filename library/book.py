from dataclasses import dataclass
from typing import Optional


@dataclass()
class Book:
  id: Optional[int] = None
  title: Optional[str] = None
  author: Optional[str] = None


  def to_dict(self) -> dict:
    return {
      'id': self.id,
      'title': self.title,
      'author': self.author,
    }

  @classmethod
  def from_dict(cls, data: dict) -> 'Book':
    return cls(
        id=int(data['id']) if 'id' in data else None,
        title=str(data['title']) if 'title' in data else None,
        author=str(data['author']) if 'author' in data else None,
    )


