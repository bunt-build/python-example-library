# Library

This project is an extremely basic microservice that can be 
used to demo how one would go about testing a service using
bunt. [Bunt](https://gitlab.com/bunt-build/bunt) is an
integration testing framework. The `master` branch of this
repository has no bunt instramentation implmented. Outside
of this repo I will be recording screen casts detailing the
usage of bunt and this repository will be used as a starting
point so viewers can follow along.

## Completed Examples

* [Implementing Bunt](https://gitlab.com/bunt-build/python-example-library/tree/integrations): Using bunt to define integration tests for a local dev environment.
* [Switching the Data Store to MongoDB](https://gitlab.com/bunt-build/python-example-library/tree/integrations-mongodb): Rewrite `library` to use MongoDB and explore what test definitions need to change.
* [Using Bunt in CI](https://gitlab.com/bunt-build/python-example-library/tree/integrations-mongodb-ci): How to run your test suite in the GitLab CI without deploying the distributed test runner.


